#include <fstream>
#include <algorithm>

using namespace std;

int cows[100000];

int N, M, C;

bool waitTimePossible(int time) {
	int buses = 1;
	int firstArrival = cows[0];
	int firstArrivalIndex = 0;
	for(int i=1;i<N;i++)
	{
		if(cows[i] - firstArrival > time || i + 1 - firstArrivalIndex > C)
		{
			buses += 1;
			firstArrival = cows[i];
			firstArrivalIndex = i;
		}
	}
	return (buses <= M);
}

int binarySearch(int low, int high) {
	if(low == high) {
        return low;
    } else if(low + 1 == high) {
		if(waitTimePossible(low)) {
            return low;
        } else {
            return high;
        }
	}
	int mid = (low+high) / 2;
	if(waitTimePossible(mid)) {
        return binarySearch(low, mid);
    }
	else return binarySearch(mid + 1, high);
}

int main() {
    ifstream inpFile;
    inpFile.open("convention.in");
    
    inpFile >> N >> M >> C;
    for (int i = 0; i < N; i++) {
        inpFile >> cows[i];
    }

    sort(cows, cows+N);

    ofstream out;
    out.open("convention.out");
    out << binarySearch(0, 1000000000) << endl;
}