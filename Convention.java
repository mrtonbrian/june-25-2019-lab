import java.util.*;
import java.io.*;
public class Convention {
    private static ArrayList<Integer> cowsArrayList = new ArrayList<>();
    // N Cows, M Buses, C Cows Per Bus
    private static int N, M, C;
    public static void main(String[] args) throws FileNotFoundException, IOException{
        Scanner inpFile = new Scanner(new File("convention.in"));

        N = inpFile.nextInt();
        M = inpFile.nextInt();
        C = inpFile.nextInt();

        for (int i = 0; i < N; i++) {
            cowsArrayList.add(inpFile.nextInt());
        }

        Collections.sort(cowsArrayList);
        
        PrintWriter pw = new PrintWriter(new FileWriter(new File("convention.out")));
        pw.println(binarySearch(0, 1000000000));
        pw.flush();

        pw.close();
        inpFile.close();
    }

    private static int binarySearch(int low, int high) {
        if (low == high) {
            return low;
        } else if (low == high+1) {
            return waitTimePossible(low) ? low : high;
        } else {
            int mid = (low + high) / 2;
            if (waitTimePossible(mid)) {
                return binarySearch(low, mid);
            } else {
                return binarySearch(mid+1, high);
            }
        }
    }

    private static boolean waitTimePossible(int time) {
        int buses = 1;
        int firstArrival = cowsArrayList.get(0);
        int firstArrivalIndex = 0;

        for (int i = 0; i < N; i++) {
            // If Adding Next Cow To Current Bus Exceeds Wait Time
            // Or If Adding Next Cow To Current Bus Exceeds Capacity
            //      Add Cow To Next Bus
            if (cowsArrayList.get(i) - firstArrival > time || i + 1 - firstArrivalIndex > C) {
                buses++;
                firstArrival = cowsArrayList.get(i);
                firstArrivalIndex = i;
            }
        }

        return buses <= M;
    }
}