import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.*;

import javax.swing.JFrame;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

public class AnalogClock extends JComponent {
    private static final int WIDTH = 500;
    private static final int HEIGHT = WIDTH + 100;

    private static final int tickMarkThickness = 2;

    private static final int MINUTETICKMARKLENGTH = 10;
    private static final int HOURTICKMARKLENGTH = 20;
    private static final int THIRDHOURTICKMARKLENGTH = 30;

    private static String TIMEZONEOFFSETPATTERN = "(([+-]([01]\\d|2[0-4]))(:[0-5]\\d)?)";
    private static Pattern TIMEZONEHOURPATTERN = Pattern.compile("([+-]([01]\\d|2[0-4]))");
    private static Pattern TIMEZONEMINUTEPATTERN = Pattern.compile("(:[0-5]\\d)");

    private static int timezoneOffset[] = new int[] { -7, 0 };

    private static final Font THIRDHOURFONT = new Font("Times", Font.BOLD, 30);
    private static final Font TIMESTAMPFONT = new Font("Times", Font.BOLD, 50);

    AnalogClock() {
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(WIDTH, HEIGHT + 30);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.getContentPane().setBackground(Color.gray);
        AnalogClock clock = new AnalogClock();
        frame.add(clock);

        JMenuBar menuBar = new JMenuBar();
        JMenuItem timezoneButton = new JMenuItem("Set Timezone");
        timezoneButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                while (true) {
                    String s = (String) JOptionPane.showInputDialog(frame, "Timezone Offset In Format\n+-XX:XX");
                    try {
                        if (s.matches(TIMEZONEOFFSETPATTERN)) {
                            Matcher matcher = TIMEZONEHOURPATTERN.matcher(s);
                            matcher.find();
                            System.out.println(s);
                            timezoneOffset[0] = Integer.parseInt(matcher.group(1));
                            System.out.println("ASFASDF");
                            matcher = TIMEZONEMINUTEPATTERN.matcher(s);
                            matcher.find();
                            timezoneOffset[1] = Integer.parseInt(matcher.group(1).substring(1));
                            return;
                        }
                    } catch (IllegalStateException state) {
                        continue;
                    } catch (NullPointerException nptr) {
                        return;
                    }
                }
            }
        });
        menuBar.add(timezoneButton);
        frame.setJMenuBar(menuBar);
        frame.setVisible(true);
    }

    public void paintComponent(Graphics g) {
        Graphics2D graphics2d = (Graphics2D) g;
        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Ellipse2D clockFace = new Ellipse2D.Double(0, 0, WIDTH, WIDTH);
        graphics2d.setColor(Color.white);
        graphics2d.fill(clockFace);
        graphics2d.setStroke(new BasicStroke(4));

        // Create Minute Tick Marks
        for (int angle = 0; angle < 360; angle += 6) {
            if (angle % 30 != 0) {
                createTickMark(g, MINUTETICKMARKLENGTH, Color.black, (double) angle, WIDTH / 2. - MINUTETICKMARKLENGTH,
                        tickMarkThickness);
            }
        }

        // Create Hour Tick Marks
        for (int angle = 0; angle < 360; angle += 30) {
            if (angle % 90 != 0) {
                createTickMark(g, HOURTICKMARKLENGTH, Color.blue, (double) angle, WIDTH / 2. - HOURTICKMARKLENGTH,
                        tickMarkThickness);
            }
        }

        // Create Tick Marks For Hours W/ Multple 3
        for (int angle = 0; angle < 360; angle += 90) {
            createTickMark(g, THIRDHOURTICKMARKLENGTH, Color.red, angle, WIDTH / 2. - THIRDHOURTICKMARKLENGTH,
                    tickMarkThickness);
        }

        TimeStamp timeStamp = getTimeStamp();
        // Note We Subtract 90 because We want 12:00 to be at the top, not the right
        // side
        // Proportion of Circle for Proportion of 12-Hr Period + Proportion of Hour
        double hourAngle = timeStamp.hours * 30. + timeStamp.minutes / 60. * 30. - 90.;
        drawHand(graphics2d, Color.black, 7, WIDTH * .3, hourAngle);
        // Note: Max For Minutes Angle is 59 * 6 + 59 * 6 / 60 = 359.9 degrees
        double minuteAngle = timeStamp.minutes * 6.0 + 6.0 * timeStamp.seconds / 60.0 - 90;
        drawHand(graphics2d, Color.blue, 5, WIDTH * .45, minuteAngle);
        // Max for Second Angle is 59 * 6 + 999999 / 1000000 * 6 = 359.999994
        double secondAngle = timeStamp.seconds * 6.0 + timeStamp.millis / 1000000.0 * 6.0 - 90;
        drawHand(graphics2d, Color.red, 3, WIDTH * .375, secondAngle);

        // Adds Text For Every 3rd Hour
        g.setFont(THIRDHOURFONT);
        FontMetrics metrics = g.getFontMetrics(THIRDHOURFONT);
        g.setColor(Color.black);
        g.drawString("12", WIDTH / 2 - metrics.stringWidth("12") / 2, THIRDHOURTICKMARKLENGTH + 30);
        g.drawString("6", WIDTH / 2 - metrics.stringWidth("6") / 2, WIDTH - THIRDHOURTICKMARKLENGTH - 10);
        g.drawString("3", WIDTH - THIRDHOURTICKMARKLENGTH - 30,
                WIDTH / 2 + metrics.getHeight() / 2 - metrics.getHeight() / 8);
        g.drawString("9", THIRDHOURTICKMARKLENGTH + 10, WIDTH / 2 + metrics.getHeight() / 2 - metrics.getHeight() / 8);

        // Draws Text At Bottom
        g.setFont(TIMESTAMPFONT);
        g.setColor(Color.blue);
        metrics = g.getFontMetrics(TIMESTAMPFONT);
        g.drawString(timeStamp.toString(), WIDTH / 2 - (metrics.stringWidth(timeStamp.toString())) / 2, WIDTH + 50);

        repaint();
    }

    private void drawHand(Graphics2D g, Color color, int thickness, double length, double angle) {
        double x = Math.cos(angle * Math.PI / 180) * length + WIDTH / 2.;
        double y = Math.sin(angle * Math.PI / 180) * length + WIDTH / 2.;

        g.setStroke(new BasicStroke(thickness));
        g.setColor(color);
        Line2D line = new Line2D.Double(WIDTH / 2., WIDTH / 2., x, y);
        g.draw(line);
    }

    private void createTickMark(Graphics g, int length, Color color, double angle, double distanceFromCenter,
            int thickness) {
        double x1 = Math.cos(angle * Math.PI / 180) * distanceFromCenter + WIDTH / 2;
        double y1 = Math.sin(angle * Math.PI / 180) * distanceFromCenter + WIDTH / 2;

        double x2 = Math.cos(angle * Math.PI / 180) * (length + distanceFromCenter) + WIDTH / 2;
        double y2 = Math.sin(angle * Math.PI / 180) * (length + distanceFromCenter) + WIDTH / 2;

        Graphics2D graphics2d = (Graphics2D) g;
        graphics2d.setColor(color);
        graphics2d.setStroke(new BasicStroke(thickness));
        graphics2d.draw(new Line2D.Double(x1, y1, x2, y2));
    }

    private static TimeStamp getTimeStamp() {
        long currTime = System.currentTimeMillis();
        long totalSeconds = currTime / 1000;
        long totalMinutes = totalSeconds / 60;
        int currSeconds = (int) totalSeconds % 60;
        int currMinutes = (int) totalMinutes % 60 + timezoneOffset[1];
        int currHours = (int) ((totalMinutes / 60) % 24) + timezoneOffset[0];

        if (currHours > 12) {
            currHours -= 12;
        }

        return new TimeStamp(currHours, currMinutes, currSeconds, (int) (currTime % 1000l));
    }

    static class TimeStamp {
        int millis;
        int seconds;
        int minutes;
        int hours;

        TimeStamp(int h, int m, int s, int mi) {
            this.seconds = s;
            this.minutes = m;
            this.hours = h;
            this.millis = mi;
        }

        public String toString() {
            return String.format("%s:%s:%s",
                    (Integer.toString(hours).length() == 2) ? Integer.toString(hours) : ("0" + Integer.toString(hours)),
                    (Integer.toString(minutes).length() == 2) ? Integer.toString(minutes)
                            : ("0" + Integer.toString(minutes)),
                    (Integer.toString(seconds).length() == 2) ? Integer.toString(seconds)
                            : ("0" + Integer.toString(seconds)));
        }
    }
}